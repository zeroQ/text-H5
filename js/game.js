function open(){
		
		
		var scene = document.querySelector('.scene'); 		//获取场景
		var pillow = document.querySelector('.pillow'); 	//获取枕头
		var moveX = 0;										//枕头的横向位移
		var moveY = 0;										//枕头的垂直位移
		var showScore = document.querySelector('.score');
	
		document.cssText = 'cursor : pointer';
	
		document.addEventListener('contextmenu',function(e){	//禁止移动端长按弹出菜单栏
			e.preventDefault();
		});
	
		/****************************提示***********************************/
	
		var tips = document.querySelector('.tips');
		var toFloor = scene.clientHeight - (pillow.children[0].clientHeight + pillow.children[0].offsetTop);	//视口顶部到地面的距离
	
		$('.pillow').animate({			//初始化枕头掉落到地面
			top : toFloor + 'px'
		},1000,'linear');
			
			
			
		var timer = 1;
		var onlyOneInit = true;													//限制重复触发init()
		tips.addEventListener('touchend',function(){							//触发提示
			this.children[0].firstElementChild.src = 'img/game/tips3.png';
			if(timer == 2){
				this.style.display = 'none';
				timer = null;
			}
			if(onlyOneInit){
				init();	
				onlyOneInit = false;
			}									// 初始化
			timer++;
			
		})
		
		
		
		/****************************能量条***********************************/
		
		var enegry = document.querySelector('.enegry');
		
		var enegryBarWidth = enegry.clientWidth + 52;						//能量条背景总宽度
		
		enegry.style.backgroundPositionX = -enegryBarWidth + 'px';			//初始化能量条的位置
		enegry.style.backgroundPositionY = 2 + 'px';	
		var ebpx;		// 能量条定位									
		document.addEventListener('touchstart',enegryManage);				//开始充能
		
		var enegryNumber = document.querySelector('.number');    
		
		

		//画能量条数值
		var num = 0;
		var context = enegryNumber.getContext('2d');
		context.font = 'italic bold 40px Arial';
		context.textAlign = 'center';
		context.fillStyle = '#ffec40';
		context.fillText('0',25,35);
		
		function enegryManage(){
			if(window.event.touches.length == 1){	//防止两只手指触发
				enegryStop = setInterval(function(){
					ebpx = parseInt(enegry.style.backgroundPositionX);
					enegry.style.backgroundPositionX = ebpx + 3 + 'px';
					enegryNumber.style.left = enegryNumber.offsetLeft + 3 + 'px';
					if(ebpx >= 0){												//防止能量条背景暴露
						enegry.style.backgroundPositionX = 0 + 'px';
						clearInterval(enegryStop);								//停止能量增加
						onlyOneInit = false;									//限制重复触发init()
						init();
					}
					
					if(enegryNumber.offsetLeft >= enegry.clientWidth){			//防止能量数值位移超出能量条的宽度
						enegryNumber.style.left = enegry.clientWidth + 'px';
					}
					
					if(num >= 100 ||enegry.style.backgroundPositionX == 0){		//能量条满了数值也要为100
						num = 100;
					}
					//画能量条数值
					str = num + '';
					context.clearRect(0,0,80,40);
					context.fillText(str,26,35);
					num++;
				},20);
			}
		}
		
		
		document.addEventListener('touchend',function(){				//充能完后
			clearInterval(enegryStop);									//结束能量持续增加的定时器
			enegryNumber.style.display = 'none';						//能量数值隐藏
			document.removeEventListener('touchstart',enegryManage);
		});
	
		
	
		
	

		/****************************初始化***********************************/
		
		var sceneStop;
		
	  	function init(){

			$('.pillow').animate({			//充能后枕头慢慢上升和横移动
				top : 80 + 'px',
				left : 50 + 'px'
			},1500,'linear');
 			
	 		sceneStop = setInterval(function(){
				gameStart();
			},20);
	  	}
	  	

		/****************************游戏开始***********************************/
		
		var ending = document.querySelector('.ending');				//结束时的跑道
		var end;
		function gameStart(){
		
			
			ebpx = parseInt(enegry.style.backgroundPositionX);

			if (ebpx <= (-enegryBarWidth)){												//能量条清空时
				clearInterval(sceneStop);												//场景停止运动
				document.removeEventListener('click',jump);								//阻止枕头落地还能跳
				document.removeEventListener('touchstart',enegryManage);				//阻止枕头落地能量还继续增加
																			
				end = setInterval(gameEnd,20);
				
				return false;
			}
			
			
			
			if (moveX >= (scene.offsetWidth - document.body.clientWidth)) moveX = 0;	//使场景一直循环移动
			
			
			
			moveX += 3;
			scene.style.transform = 'translateX('+ (-moveX) + 'px)';					//使场景横向移动
			
			if(ebpx >= 0){																//防止能量条背景暴露
				enegry.style.backgroundPositionX = 0 + 'px';
				clearInterval(enegryStop);
			}else{
			
				enegry.style.backgroundPositionX = ebpx + 1 + 'px';						//飞行状态下能量缓慢增加	

			}
				
				
				
			increment = pillow.offsetTop;	
			if (increment < (scene.clientHeight - pillow.clientHeight)) {				//枕头向下跌落
				increment += 2;
				pillow.style.top = increment + 'px';
			}else{																	
				clearInterval(sceneStop);
				document.removeEventListener('click',jump);								//阻止枕头落地还能跳
				document.removeEventListener('touchstart',enegryManage);						//阻止枕头落地能量还继续增加
				enegry.style.backgroundPositionX = -enegryBarWidth + 'px';						//清空能量
				
				$('.pillow').animate({
					top : (scene.clientHeight - pillow.clientHeight) + 'px'				//阻止枕头向下位移时超出视口高度
				},4000);
																							
				ending.style.transform = 'translateX(' + (-document.body.clientWidth) + 'px)';	//结束跑道滑动出现
				
				$('.pillow').css({
					transition : 'transform 1s linear'
				},1000);
				
				pillow.style.transform = 'translateX(100px)';							//结束时枕头向前滑动
				score = moveX;
				drawScore(score);
				showScore.style.display = 'block';
			}
			
			
		}
		

		
		function gameEnd(){											
			if(pillow.offsetTop >= toFloor){								//让枕头缓慢地掉落地面
				clearInterval(end);
				clearInterval(sceneStop);
				$('.pillow').animate({
					top : toFloor + 'px'
				},4000);
				ending.style.transform = 'translate(' + (-document.body.clientWidth) + 'px)';	//结束跑道滑动出现
				$('.pillow').css({
					transition : 'transform 1s linear'
				},1000);
				pillow.style.transform = 'translateX(100px)';							//结束时枕头向前滑动
				score = moveX;
				drawScore(score);
				showScore.style.display = 'block';
				return false;
			}
			pillow.style.top = pillow.offsetTop + 10 + 'px';
		}
		
	
		/****************************跳***********************************/
		
		document.addEventListener('click',jump);
		var preventFirstJump = true;
		function jump(){
			
			if(pillow.style.top >= toFloor) return false;					//一旦枕头掉到地上就不能再跳
			if(pillow.offsetTop < 0){
				pillow.style.top = 0 + 'px';			//防止枕头跳跃时超出视口顶部
			} 
			
			if(preventFirstJump == false){				// 防止蓄力时触发起跳消耗能量
				var	jumpHeight = pillow.offsetTop - 50 + 'px';				//飞行时每次跳跃的高度
					
					$('.pillow').animate({
						top : jumpHeight
					},150);
			}
			
			preventFirstJump = false;

			ebpx = parseInt(enegry.style.backgroundPositionX);
			
			enegry.style.backgroundPositionX = ebpx - 45 + 'px';			//飞行时每次跳跃消耗的能量
		}


		/****************************分数成绩***********************************/
		var score;
		var scoreBoard = document.querySelector('.inner>.top');
		var scoreDesciption = document.querySelector('.inner>.bottom');
		var currentScore = scoreBoard.getElementsByTagName('canvas')[0];
		var historyScore = scoreBoard.getElementsByTagName('canvas')[1];
		var currentContext = currentScore.getContext('2d');
		var historyContext = historyScore.getContext('2d');
		currentScoreWidth = parseInt(window.getComputedStyle(currentScore,null).width);
		currentScoreHeight = parseInt(window.getComputedStyle(currentScore,null).height);
		function drawScore(distance){
			const unit = 3.33;
			var str = Math.ceil((distance + pillow.offsetWidth + 50) * 2 * unit);	//计算分数
			midPlaceX = currentScoreWidth/2 + 75;
			midPlaceY = currentScoreHeight/2 + 80;
			
			scoreDesciption.children[0].innerHTML = str;

			scoreDesciption.children[1].innerHTML = parseInt((distance + pillow.offsetWidth + 50) / scene.offsetWidth * 100) + '%';
			/***
			 * 
			 *	画当前得分
			 *
			 */
			currentContext.font = 'italic bold 60px Arial';
			currentContext.textAlign = 'center';
			currentContext.fillStyle = '#ffec40';
			currentContext.fillText(str,midPlaceX,midPlaceY,200);
			/***
			 * 
			 *	画历史得分
			 *
			 */
			historyContext.font = 'italic bold 60px Arial';
			historyContext.textAlign = 'center';
			historyContext.fillStyle = '#ffec40';
			if(!localStorage.getItem('score')){
				localStorage.setItem('score',str);
			 }else{
				if(str > localStorage.getItem('score')){					
					localStorage.setItem('score',str);
					historyContext.fillText(str,midPlaceX + 15,midPlaceY,200);
				}else{	
					oldScore = localStorage.getItem('score');	
					historyContext.fillText(oldScore,midPlaceX + 15,midPlaceY,200);
				}
			}
			 
			
		}
		
		
}


