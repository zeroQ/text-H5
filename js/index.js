window.onload = function(){
	/*
	 * 参与游戏按钮
	 * 
	 */
	var joinGameBtn = document.querySelector('.join-game');
	var officialAccountMask = document.querySelector('.official-account-mask');
	joinGameBtn.addEventListener('touchstart',function(){
		officialAccountMask.style.display = 'block';
	});
	var closeOffAccBtn = officialAccountMask.querySelector('.close-btn');
	
	closeOffAccBtn.addEventListener('touchstart',function(){
		officialAccountMask.style.display = 'none';
		selectPillowPage.style.zIndex = '50';
	});
	
	/*
	 *游戏规则按钮 
	 * 
	 */
	var closeRulesBtn = document.querySelector('.close-rules-btn');
	var gameRulesMask = document.querySelector('.game-rules-mask');
	closeRulesBtn.addEventListener('touchstart',function(){
		gameRulesMask.style.display = 'none';
	});
	var openRulesBtn = document.querySelector('.game-rules');
	openRulesBtn.addEventListener('touchstart',function(){
		gameRulesMask.style.display = 'block';
	});
	
	/*
	 * 选枕头
	 *
	 *
	 */
	var selectPillowPage = document.querySelector('.select-pillows-page');
	var prevBtn = selectPillowPage.querySelector('.prev');
	var nextBtn = selectPillowPage.querySelector('.next');
	var allPillows = selectPillowPage.querySelector('.watch-port>ul');
	var onePillowWidth = allPillows.children[0].clientWidth;
	var pillowIndex = 0;	
	allPillows.style.left = 0 + 'px';

	var preventTouch = true;
	
	//防止轮播混乱
	allPillows.addEventListener('transitionend',function(){
		preventTouch = true;
	});
	
	
	//上一款枕头
	prevBtn.addEventListener('touchstart',prevPillow);
			
	function prevPillow(){
		//改变枕头下标
		if(pillowIndex > 0 && pillowIndex <= allPillows.children.length-1) 	pillowIndex--;
		
		//如果动画结束则允许移动枕头
		if(preventTouch){
			preventTouch = !preventTouch;
			if(parseInt(allPillows.style.left) >= 0){
				allPillows.style.left = 0 + 'px';
				preventTouch = true;
			}else{
				allPillows.style.left = allPillows.offsetLeft + onePillowWidth + 'px';
			}
		}
	}
	
	//下一款枕头
	nextBtn.addEventListener('touchstart',nextPillow);
	
	function nextPillow(){
		//改变枕头下标
		if(pillowIndex >= 0 && pillowIndex < allPillows.children.length-1) pillowIndex++;
		
		//如果动画结束则允许移动枕头
		if(preventTouch){
			preventTouch = !preventTouch;

			if(parseInt(allPillows.style.left) <= - (onePillowWidth*(allPillows.children.length-1))){
				allPillows.style.left = -(onePillowWidth*(allPillows.children.length-1)) + 'px';
				preventTouch = true;
			}else{
				allPillows.style.left = allPillows.offsetLeft - onePillowWidth + 'px';
			}
			
		}
	}
	//滑动屏幕选择枕头
	var selectPillowPage = document.querySelector('.select-pillows-page');
	var startX,startY,endX,endY;
	
	selectPillowPage.addEventListener('touchstart',function(e){
		e.stopPropagation();
		startX = parseInt(e.changedTouches[0].clientX);
		startY = parseInt(e.changedTouches[0].clientY);
	})
	
	selectPillowPage.addEventListener('touchend',function(e){
		e.stopPropagation();
		endX = parseInt(e.changedTouches[0].clientX);
		endY = parseInt(e.changedTouches[0].clientY);
		if( (startX < endX) && Math.abs(startY-endY) <= 50 ) prevPillow();
		if( (startX > endX) && Math.abs(startY-endY) <= 50 ) nextPillow();
	});

	//通过url传递枕头下标
	var sureSelectBtn = document.querySelector('.sure-select');

	sureSelectBtn.addEventListener('touchstart',function(){
		this.href = 'game.html?' + 'pi=' + pillowIndex;
	});
	
	
}



